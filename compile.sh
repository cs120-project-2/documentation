#!/bin/sh

pdflatex doc.tex &&
  pdflatex doc.tex &&
  biber doc &&
  pdflatex doc.tex &&
  git clean -xfe "doc.pdf"
